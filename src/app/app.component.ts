import { Component, OnInit } from '@angular/core';
import { AuthConfig, OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  loginUrl : 'https://login.microsoftonline.com/e4e34038-ea1f-4882-b6e8-ccd776459ca0/oauth2/authorize',
  clientId : '8cdd9142-f59b-47dc-b9c9-832becfc6ce0',
  resource : 'https://celebaltech.com/15908687-39df-4e58-a671-84a9418ae8e3',
  logoutUrl :'https://login.microsoftonline.com/e4e34038-ea1f-4882-b6e8-ccd776459ca0/oauth2/logout',
  redirectUri : window.location.origin + '/',
  scope : 'openid',
  oidc : true

}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private oauthService: OAuthService) {
    this.configureWithNewConfigApi();
  }

  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }


  ngOnInit() {

  }

  public login() {
    this.oauthService.initImplicitFlow();
    this.oauthService.getAccessToken();
  }

  public logoff() {
    this.oauthService.logOut();
  }

}
